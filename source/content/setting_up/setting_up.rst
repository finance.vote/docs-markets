Setting up
==================

.. contents:: Table of Contents
   :depth: 2


Locally
----------------------
Prerequisites:

* Created an account on GitLab via invite
* Ganache-cli installed
* Truffle suite installed
* Code editor installed



1. In the contracts terminal::

    truffle compile
    truffle deploy

2. Run

    ``ganache-cli -d -i 333 --chainId  333``

3. In the trollbox terminal::

    git checkout develop
    git pull
    yarn install
    ./scripts/import-contracts.sh

4. In the main trollbox folder create a file **.env.local** and put in there::

    SKIP_PREFLIGHT_CHECK=true
    ESLINT_NO_DEV_ERRORS=true
    REACT_APP_LOCAL_PROVIDER_URL='ws://207.154.210.94:8545'

5. Run application::

    yarn start


On Remote Server
------------------
Prerequisites:

* Created an account on GitLab via invite
* Code editor installed
* On MacOS: wget install

Testing server address: 207.154.210.94:8545

1. In the trollbox terminal::

    git checkout develop
    git pull
    yarn install

2. Update contracts, see `here`_.

    .. _here: https://financevote.readthedocs.io/projects/markets/en/latest/setting_up/update_contacts.html

3. In the main trollbox folder create a file **.env.local** and put in there::

    REACT_APP_LOCAL_PROVIDER_URL='http://207.154.210.94:8545’

4. Run application::

    yarn start
