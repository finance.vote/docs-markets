.. _markets-vote-before-setting-up:

Before setting up
--------------------------

Clone following repositories and put them both in one main folder:\

`Contracts`_

.. _Contracts: https://gitlab.com/finance.vote/contracts

`Trollbox`_

.. _Trollbox: https://gitlab.com/finance.vote/trollbox

